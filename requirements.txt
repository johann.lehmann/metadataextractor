rdflib>=6.0.1
networkx>=2.6.3
matplotlib>=3.4.3
SpeechRecognition>=3.8.1
moviepy>=1.0.3
tensorflow>=2.8.0
tensorflow-gpu>=2.8.0
opencv-python>=4.5.5.62
cvlib>=0.2.7
numpy>=1.20.3
pytesseract>=0.3.8
argparse>=1.4.0
requests>=2.25.1
textract>=1.5.0
wget>=3.2
chardet>=3.0.4
tika>=1.24
urllib3>=1.26.6
seaborn>=0.11.2
sklearn
adjustText>=0.7.3
pyldavis>=3.3.1
funcy>=1.17
boto>=2.49.0
boto3>=1.21.2
google-cloud-speech==1.3.2
google-cloud-storage>=1.41.1
wave>=0.0.2
pydub>=0.25.1
PyMuPDF>=1.19.5
nltk>=3.7
flask>=2.0.1
pygments>=2.10.0
python-magic==0.4.25;sys_platform!='win32'
python-magic-bin==0.4.14;sys_platform=='win32'
pattern>=3.6
validators>=0.18.2
google_api_core==1.31.4
gast==0.5.1
fonttools==4.29.0
gensim==4.1.2
waitress
